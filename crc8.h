#ifndef _CRC8_H_
#define _CRC8_H_

#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>

uint8_t crc8(const uint8_t *src, size_t len, uint8_t polynomial, uint8_t initial_value,
	  bool reversed);

#endif
